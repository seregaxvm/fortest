#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <memory.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include "tests.h"

#define MAXTIME 3000000

void usage(void) {
    printf("\nUsage: ./fortest [test] [program]\n");
    printf("Available tests:\n");
    printf("\t--calc\n");
    printf("\t--triangle\n");
    printf("\t--integral [1|2]\n");
}

void setfd(int *in, int *out) {
    dup2(out[1], 1);
    dup2(in[0], 0);
    close(in[0]);
    close(in[1]);
    close(out[0]);
    close(out[1]);
}

int main(int argc, char *argv[]) {
    int i = 0, j = 0, status = 0;
    int pipe2ch[2], pipe2pr[2];
    pid_t pid;
    char *newargv[] = { NULL, NULL };

    char **inputdata = NULL;
    char **expecteddata = NULL;

    // check number of arguments
    if (argc < 3) {
        errno = EINVAL;
        perror("Invalid number of arguments");
        usage();
        exit(1);
    }
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"

    // set data

    if (strcmp(argv[1], "--calc") == 0) {
        inputdata = &inputdata_calc;
        expecteddata = &expecteddata_calc;
        newargv[0] = argv[2];
    } else if (strcmp(argv[1], "--triangle") == 0) {
        inputdata = &inputdata_triangle;
        expecteddata = &expecteddata_triangle;
        newargv[0] = argv[2];
    } else if (strcmp(argv[1], "--integral") == 0) {
        if (strcmp(argv[2], "1") == 0) {
            inputdata = &inputdata_integral1;
            expecteddata = &expecteddata_integral1;
            newargv[0] = argv[3];
        } else if (strcmp(argv[2], "2") == 0) {
            inputdata = &inputdata_integral2;
            expecteddata = &expecteddata_integral2;
            newargv[0] = argv[3];
        } else {
            goto invarg;
        }
    } else {
 invarg:
        errno = EINVAL;
        perror("Invalid argument");
        usage();
        exit(1);
    }

#pragma GCC diagnostic pop

    //create a copy of stdout
    int ustdout = open("/dev/null", O_WRONLY);

    dup2(1, ustdout);

    while (*(inputdata + i) != NULL) {
        // open pipes
        if ((pipe(pipe2ch) == -1) | (pipe(pipe2pr) == -1)) {
            errno = EPIPE;
            perror("Pipe");
            exit(1);
        }
        // fork child
        if (!(pid = fork())) {
            // set file descriptors
            setfd(pipe2ch, pipe2pr);

            // execute child
            execve(newargv[0], newargv, NULL);

            errno = ECHILD;
            perror("Child");
            exit(1);
        } else {
            char *str = NULL;
            size_t strsz = sizeof(char) * 50;

            // set file descriptors
            setfd(pipe2pr, pipe2ch);
            int rc = write(1, inputdata[i],
                           sizeof(char) * strlen(inputdata[i]));

            if (rc == -1) {
                perror("write");
                exit(1);
            }

            rc = 0;
            int time_pass = 0;

            while (time_pass < MAXTIME) {
                if ((rc = waitpid(pid, &status, WNOHANG)) > 0) {
                    if (WEXITSTATUS(status) != 0) {
                        write(ustdout,
                              "Abnormal program exit\n", sizeof(char) * 22);
                        exit(0);
                    }
                    break;
                }
                usleep(100);
                time_pass += 100;
            }
            if (rc == 0) {
                kill(pid, SIGKILL);
                errno = ETIME;
                perror("Maximum time exceeded");
                exit(1);
            }

            while (getline(&str, &strsz, stdin) > 0) {
                while (*str == ' ') {
                    char *tmp = NULL;

                    tmp = strndup(str, strlen(str) + 1);
                    strncpy(str, tmp + 1, strlen(tmp));
                    free(tmp);
                }
                if (expecteddata[j] == NULL) {
                    errno = ERANGE;
                    perror("too much output");
                    exit(0);
                }
                if (strcmp(str, expecteddata[j]) != 0) {
                    write(ustdout, "Wrong answer!\n", sizeof(char) * 14);
                    write(ustdout, "*******************\n", sizeof(char) * 20);
                    write(ustdout, "Input:\n", sizeof(char) * 7);
                    write(ustdout, inputdata[i],
                          sizeof(char) * strlen(inputdata[i]));
                    write(ustdout, "Output:\n", sizeof(char) * 7);
                    write(ustdout, str, sizeof(char) * strlen(str));
                    write(ustdout, "Expected output:\n", sizeof(char) * 17);
                    write(ustdout, expecteddata[j],
                          sizeof(char) * strlen(expecteddata[j]));
                    free(str);
                    exit(0);
                }
                j++;
            }
            free(str);
            i++;
        }
    }
    write(ustdout, "Right answer!\n", sizeof(char) * 14);
    return 0;
}
